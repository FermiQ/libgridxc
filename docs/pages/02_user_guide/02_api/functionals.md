title: Guide to functionals

The library provides a collection of built-in functionals, coded by
J.M. Soler and C. Balbás, and an interface to the [LibXC](https://www.tddft.org/programs/libxc/) library for a
wider selection of semi-local (LDA and GGA) functionals. Support for
the MGGA functionals in LibXC is being implemented.

## Built-in functionals

The built-in functionals are classified by `family` (LDA (or LSD), GGA, VDW) and `authors`:

### LDA/LSD

```
     'CA' or 'PZ' => LSD Perdew & Zunger, PRB 23, 5075 (1981)
           'PW92' => LSD Perdew & Wang, PRB, 45, 13244 (1992)
```

### GGA

```
           'PW91' => GGA Perdew & Wang, JCP, 100, 1290 (1994) 
            'PBE' => GGA Perdew, Burke & Ernzerhof, PRL 77, 3865 (1996)
           'RPBE' => GGA Hammer, Hansen & Norskov, PRB 59, 7413 (1999)
         'revPBE' => GGA Zhang & Yang, PRL 80,890(1998)
            'LYP' => GGA Becke-Lee-Yang-Parr (see subroutine blypxc)
             'WC' => GGA Wu-Cohen (see subroutine wcxc)
         'PBESOL' => GGA Perdew et al, PRL, 100, 136406 (2008)
           'AM05' => GGA Mattsson & Armiento, PRB, 79, 155101 (2009)
      'PBEJsJrLO' => GGA Reparametrizations of the PBE functional by
     'PBEJsJrHEG' => GGA   L.S.Pedroza et al, PRB 79, 201106 (2009) and
      'PBEGcGxLO' => GGA   M.M.Odashima et al, JCTC 5, 798 (2009)
     'PBEGcGxHEG' => GGA using 4 different combinations of criteria
```

### VDW

```
          'DRSLL' => VDW Dion et al, PRL 92, 246401 (2004)
          'LMKLL' => VDW K.Lee et al, PRB 82, 081101 (2010)
            'KBM' => VDW optB88-vdW of J.Klimes et al, JPCM 22, 022201 (2010)
            'C09' => VDW V.R. Cooper, PRB 81, 161104 (2010)
             'BH' => VDW K. Berland and Per Hyldgaard, PRB 89, 035412 (2014)
             'VV' => VDW Vydrov-VanVoorhis, JCP 133, 244103 (2010)
```

To access these functionals, one can simply call the [[setxc_family_authors(proc)]] routine:
```
  call setxc_family_authors('GGA', 'PBE')
```

The library offers also an expanded version of this routine that is
able to describe *cocktail mixings* of functionals, each with its
appropriate weight. For example,

```
   call setXC( 2, (/'GGA','GGA'/), (/'PBE','WC'/), (/1.0_dp, 0.0_dp/), (/0.75_dp,0.25_dp/) )
```
will use a cocktail of PBE and WC in which the exchange part is 100% PBE, and the correlation part is 75% PBE and 25% WC.



## LibXC interface

In [LibXC](https://www.tddft.org/programs/libxc/functionals), most
functionals are split by exchange and correlation parts. For example,
there are separate functionals for the exchange and corrrelation parts
of PBE, respectively XC_GGA_X_PBE (id 101) and XC_GGA_C_PBE (id
130). The *id* or functional number, while not human friendly, is the
recommended way of identification for full compatibility.

To tell libGridXC that a PBE functional from LibXC is desired, we can use the call:

```
  call setXC_libxc_ids ( 2, (/ 101, 130 /) )
```

Note that this combination of exchange and correlation parts can be seen as a special kind of *cocktail mixing*. Hence
the same result could be obtained by the call:

```
   call setXC( 2, (/'GGA','GGA'/), (/'LIBXC-101','LIBXC-130'/), (/1.0_dp, 0.0_dp/), (/0.0_dp,1.0_dp/) )
```

where the "author" field is a special string of the form
`LIBXC-XXXX-SYMBOL` where `-SYMBOL` is optional if `XXXX` is a valid
libXC functional id. Accepted symbols are those for which the
`functional_get_number` routines of LibXC return a valid functional
id, and are of the form `FAMILY_TYPE_NAME` (for example, `GGA_X_PBE`
-- see the complete list in the `xc_funcs.h` file generated at the
time of compilation of libXC, noting that the initial `XC_` has to be
removed). If the LibXC id is not known, it can be set to 0 and leave
the specification to the symbolic part. For example, the following
forms are equivalent:

```
LIBXC-101
LIBXC-0101
LIBXC-0-GGA_X_PBE
LIBXC-101-GGA_X_PBE
```

This form is more cumbersome, but it can be generalized if desired to extra
mixings (even with non-libxc functionals).


A handful of LibXC functionals are "exchange-correlation". For them, the specification can simply be, for example
(XC_LDA_XC_TETER93 functional, id=20):

```
  call setXC_libxc_ids ( 1, (/ 20 /) )
```

