## Copyright (C) 2017-2019 Yann Pouillon
##
## This file is part of the LibGridXC software package. For license information,
## please see the COPYING file in the top-level directory of the source
## distribution.

                    # ------------------------------------ #

#
# Top makefile
#

# Autotools parameters
ACLOCAL_AMFLAGS = -I config/m4

# Disable optional features when running 'make distcheck'
AM_DISTCHECK_CONFIGURE_FLAGS = \
  --disable-debug \
  --disable-multiconfig \
  --disable-single \
  --without-libxc \
  --without-mpi

# Subdirectories containing makefiles
SUBDIRS = src docs Testers

# Support for pkg-config (see http://pkg-config.freedesktop.org/wiki/)
pkgconfigdir = $(prefix)/lib/pkgconfig
if GRIDXC_USES_MULTICONFIG
  pkgconfig_DATA = config/data/libgridxc@PREC_SUFFIX@@ARCH_SUFFIX@.pc
else
  pkgconfig_DATA = config/data/libgridxc.pc
endif

if GRIDXC_USES_MULTICONFIG
config/data/libgridxc@PREC_SUFFIX@@ARCH_SUFFIX@.pc: config/data/libgridxc.pc
	cp $< $@
endif

# Support for SIESTA's .mk files
siestadir = $(datarootdir)/org.siesta-project
if GRIDXC_USES_MULTICONFIG
  siesta_DATA = config/data/gridxc@PREC_SUFFIX@@ARCH_SUFFIX@.mk
else
  siesta_DATA = config/data/gridxc.mk
endif
if GRIDXC_USES_LIBXC
  siesta_DATA += config/data/libxc.mk
endif

if GRIDXC_USES_MULTICONFIG
config/data/gridxc@PREC_SUFFIX@@ARCH_SUFFIX@.mk: config/data/gridxc.mk
	cp $< $@
endif

# Files to distribute in the source package
EXTRA_DIST = \
  README \
  README.md \
  libgridxc.md \
  multiconfig-build.sh \
  config/data/libgridxc.pc.in \
  config/data/libgridxc-config.yml.in \
  extra/fortran.mk \
  extra/handlers.f90 \
  src/config.sh \
  src/build.sh \
  src/makefile.alt \
  src/gridxc.mk.in \
  src/libxc.mk \
  src/top.gridxc.mk.in
#
# Custom targets
distclean-local:
	rm -f $(distdir).tar.bz2 $(distdir).tar.gz $(distdir).tar.xz
